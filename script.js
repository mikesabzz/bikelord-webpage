   fetch("./bike.json")
        .then(res => res.json())
        .then(data => {
            const html = data.map(item => {
                return `
                <div class="bikeshop-card border border-dark bg-white m-3 p-4">
                <li><img class="border border-dark" width="75%" src=${item.image}></li>
                <li><img src=${item.logo}></li>
                <li><a href=${item.website}><h3>${item.shopname}</h3></a></li>
                <li><h4>${item.contact}</h4></li>
                <li><h4> <span class="glyphicon glyphicon-earphone"></span> ${item.phone}</h4></li>
                <li><h4><span class="glyphicon glyphicon-home"></span> ${item.address}</h4></li>
                <li><a class="btn btn-danger" href=${item.facebook}>Facebook</a><a class="btn btn-danger ml-1" href=${item.instagram}>Instagram</a></h4></li>
                </div>
                `
            })

            
            const docs = document.querySelector('.bikeshop-container')
            docs.innerHTML = html
            
        })

